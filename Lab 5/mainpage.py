## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_port Portfolio Details
#  This is my ME405 Portfolio. See individual modules for detals.
#
#  Included modules are:
#  * Lab0x00 (\ref sec_lab0)
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x02 (\ref sec_lab2)
#  * Lab0x03 (\ref sec_lab3)
#  * Lab0x04 (\ref sec_lab4)
#  * Lab0x05 (\ref sec_lab5)
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%200/
#  * Documentation: \ref Lab0
#
#  @section sec_lab1 Lab0x01 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/Vendotron.py
#  * Documentation: Vendotron.py\ref Lab 1
#  * HMTL: Lab1 https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/html/
#
#  @section sec_lab2 Lab0x02 Documentation
#  * Source: https://bitbucket.org/amart426/amart426.bitbucket.io/src/master/me_405/
#  * Documentation: Lab 2 (lab2main.py\ref)
#  * HMTL: Lab2 https://bitbucket.org/amart426/me-405-labs/src/master/Lab%202/html/
#
# @section sec_lab3 Lab0x03 Documentation
# * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%203/
# * Documentation: main.py\n
# * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%203/html/
#
#
#  @section sec_intro3 Lab3: Introduction
#  The project main goal is to measure voltage in a board and sennd it the PC. This
#  project consisted of runnig a Nucleo-L476RG with a main.py and UI_front.py code to interact
#  with the user. The main.py script  waits for the user to send character 'G'
#  and then it will get ready to start measuring.
#  The user must press the blue button connected to Pin C13 to start
#  saving the voltage in an array. When finish measuring, the array 
#  will then be send back to the PC or UI_front.py.
#  The UI_front then receive and store the data send via serial port. The
#  final result is a plot of the data.
#  @subsection sec_notes Comments
#  In the process of completing this lab I had some issues getting good data
#  from the adc. When I press the blue button, it starts reading from a high value. 
#  I try using different pins. The best one was Pin A0. This would give a better
#  a smoother curve. After doing all of changes and playing around with different
#  pins and timers and frequencies. My UI_front.py receive empy data. My UI_front
#  receive empy data. I not sure if my I mess my Nucleo but now is not respoding. 
#
# @section sec_lab4 Lab0x04 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab04/
# * Documentation: main.py & mcp9808.py\n
#
#  @subsection sec_intro4 Lab4: Introduction
#  The main goal of this project is to measure the ambient temperature and 
#  the STM32 Microcontroller core temperature. The ambient temperature is measure
#  using a MCP9808 sensor. The pyb.I2C() allows the interface between the
#  the sensor and the STM32 Microcontroller. The physical connnection is shown
#  below. The microcontroller core temperature is measure
#  using the pyb.ADCAll() method. The main.py file runs for 8hrs and 30 mins
#  and records the time, ambient temperature, and core temperature and finally
#  saves the data to a csv file. The data from the csv file is plotted
#  separately.
#  @subsection sec_notes Comments
#  One big obstacle for completing this assignment was having the MCP9808 sensor
#  arrive late. One team member had the MCP9808 sensor for the time being, therefore
#  one member collected all the data for the sake of time. The other was in
#  in charged for documentating. Other issues included the computer powering
#  down and restarting collection data. The data was also plotted separately. This
#  facilitated data and plot formating. 
#  @subsection  sec_phycon Physical-Connection
#  @image html SensorCon.png 
#  @subsection  sec_plots  Temperature vs.Time Plot
#  The plot below shows the ambient and internal temperatures as functions of time.
#  This test was performed beginning at 5:53 PM in Matthew Pfeiffer's bedroom. Data
#  points were taken each minute for 510 minutes.
#  @image html  TempPlot.png
#  @author Adan Martinez 
#
#  @date January 26, 2021
#
# @section sec_lab5 Lab0x05 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab05/
# * Documentation: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%205/
# * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%205/html/
#  @subsection sec_intro Introduction
#  The main goal of this project is to analyze a balancing platform. The 
#  mechanics of the system consists of two motors and each connected to a 
#  lever arm. A pushing rod connects the lever arm to the platform. When a 
#  torque is applied, the lever arm rotates, and the motion is transfer to the 
#  platform via the pushing rod. For the analysis the ball is taken into 
#  account. The analysis takes part by solving for kinetic equations that 
#  relates the motion of the lever arm pivoting point to the base of the 
#  platform. To approach the following assumptions were made. The system was 
#  model as a pair of balancing beams that each move in one degree of freedom.
#  We assume small angle approximation. We assume the platform has the most 
#  significant mass and inertia and ignore that of the lever arm and push rod.
#  For the ball we assume it rolls without slipping.  The end result is two 
#  high order differential equations relating the torque from the motor, ball's
#  acceleration, and angular acceleration of the platform. Also, the velocities 
#  and position of the platform and lever arm.  The equations are then 
#  manipulated to a matrix form. Please refer to Analysis page for further 
#  detail. 
#  @author Matthew Pfeiffer
#  @author Adan Martinez 
#
#  @date February 16, 2021
#
## @page Lab5Analysis Analysis
#  @image html Analysis.jpeg
#  @image html Analysis1.jpeg
#  @image html Analysis2.jpeg
#  @image html Analysis3.jpeg
#  @image html Analysis4.png
#  @image html Analysis5.png
#  @image html Analysis6.jpg