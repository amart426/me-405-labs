## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_port Portfolio Details
#  This is my ME405 Portfolio. See individual modules for detals.
#
#  Included modules are:
#  * Lab0x00 (\ref sec_lab0)
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x02 (\ref sec_lab2)
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%200/
#  * Documentation: \ref Lab0
#
#  @section sec_lab1 Lab0x01 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/
#  * Documentation: Vendotron.py\ref Lab 1
#  * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/html/
#
#  @section sec_lab2 Lab0x02 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%202/
#  * Documentation: Lab 2 (lab2main.py\ref)
#  * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%202/html/
#  @section sec_intro Introduction
#  The project main goal is to simulate a reaction time testing device. This
#  project consisted of using a Nucleo-L476RG and lab2main.py code to interact
#  with the user. The interaction was through using the blue button on the nucleo
#  and set it up as trigger for the interrupt callback. 
#  The while loop, first the rand_delay() returns a
#  randomtime between 2 and 3 seconds, then turns on the LED connected 
#  to the microcontroller pinPA5 and start timing with a timer that 
#  counts microseconds. The LED stays on for one second. The external 
#  interrupt when pressed, and activate the function myCallback(), 
#  it causes a falling edge on the pin, so an 
#  interrupt is triggered by the falling edge. The interrupt callback 
#  function then reads the timer to see how many microseconds have 
#  elapsed since the LED was turned on. The process cycles between 
#  turning the LED off and on and measuring reaction times. The program
#  is designed to stop when the user presses Ctrl-C. When the program 
#  is stopped, the average reaction is displayed.  An errormessage is 
#  printed if no reaction time was measured.
#
#  @author Adan Martinez 
#
#  @date January 26, 2021