#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Vendotron.py

@brief      A program that tests the user's reaction time.
@details    This file cosists of a two functions and one while loop. myCallback
            is a function that serves as the interrupt callback, which runs within 
            a fewmicroseconds of the trigger condition. The trigger condition is 
            whenever the pinPC13 is pressed. The rand_delay simply returns a random
            time between 2 and 3seconds. The while loop has the task to wait a 
            randomtime between 2 and 3 seconds, then turns on the LED connected 
            to the microcontroller pinPA5 and start timing with a timer that 
            counts microseconds. The LED stays on for one second. The external 
            interrupt when pressed, it causes a falling edge on the pin, so an 
            interrupt is triggered by the falling edge. The interrupt callback 
            function then reads the timer to see how many microseconds have 
            elapsed since the LED was turned on. The process cycles between 
            turning the LED off and on and measuring reaction times. The program
            is designed to stop when the user presses Ctrl-C. When the program 
            is stopped, the average reaction is displayed.  An errormessage is 
            printed if no reaction time was measured.

@author     Adan Martinez
@date       January 26,2021
"""

# Import any required modules
import pyb
import micropython
import utime
import random

# Global variable
global aveg_tim  
global start_count


# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(200)

## Create a timer object (timer 2) in general purpose counting mode
#  at 10 Hz
tim = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF) # make sure to omit
period = 2147483647

## Pin object to use for blinking LED. Attached to PA5
myLED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Callback function to run when timer overflows
#  timSource is the timer object triggering the callback
stop_count = 0
def myCallback(timSource):
    '''
    @brief         A function that serves as the interrupt.
    @detail        The interrupt callback function then reads the timer to see 
                   how many microseconds have elapsed since the LED was turned on.
                   The paramenter can be ignored.
    '''
    global stop_count
    stop_count = tim.counter()
                           

def rand_delay():
    '''
    @brief         A function that returns a random time.
    @detail        This function returns a random time between 2 and 3 seconds.
                   This time is used to set long the while loop delays before 
                   start taking measurements.
    '''
    tim_delay = (random.random() + 2)
    return tim_delay

## Setting Up Pin for Interruptor    
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
             pyb.Pin.PULL_UP,              # Activate pullup resistor
             myCallback)                   # Interrup Service Routine

# Main program While loop
state = 0      #Setting up state
aveg_tim = []  #List for saving average reaction time
while True:
    '''
    @brief      Impletements the main task of the program.
    @detail     The while loop has the task to wait a 
                randomtime between 2 and 3 seconds, then turns on the LED connected 
                to the microcontroller pinPA5 and start timing with a timer that 
                counts microseconds. The LED stays on for one second. The external 
                interrupt when pressed, it causes a falling edge on the pin, so an 
                interrupt is triggered by the falling edge. The interrupt callback 
                function then reads the timer to see how many microseconds have 
                elapsed since the LED was turned on. The process cycles between 
                turning the LED off and on and measuring reaction times. The program
                is designed to stop when the user presses Ctrl-C. When the program 
                is stopped, the average reaction is displayed.  An errormessage is 
                printed if no reaction time was measured.
    '''
    try:
        if state == 0:
            print('Get Ready...')
            # Setting objects equal to zero
            stop_count = 0
            start_count = 0
            
            time_delay = rand_delay()  
            utime.sleep(time_delay)    #Delaying for a time between 2<t<3seconds.
            tim.counter(0)  # Set counter value to zero
            start_count = tim.counter()  # Start timer
            state = 1  # Next is state 1
            
        elif state == 1:
            myLED.high()
            print('LED ON')
            utime.sleep(1)   #LED is ON for 1 second
            myLED.low()    # LED OFF
            print('LED OFF')
            if stop_count != 0:
                myLED.low()
                print('LED OFF')
                delta = stop_count - start_count # Takes the difference from when
                                                 # timer was started to when it stopped
                time = (delta/1000000)  # Converts the timer to a time in seconds
                #print('Your time is: ' + str(time))
                aveg_tim.append(time)  # Adds the measurement to the list.
                state = 0
            else:
                print('Over limit...')
                state = 0
    ## Only activate when users presses CRTL-C.
    except KeyboardInterrupt:
        if sum(aveg_tim) == 0:  # Takes cares of an empty average time list.
            print('error...No time recorded!')
            break
        else:
            average_time = sum(aveg_tim)/len(aveg_tim)
            print(aveg_tim)
            print('Your average reaction time is: ' + str(average_time) + 'seconds')
        break

