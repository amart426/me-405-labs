var lab2main_8py =
[
    [ "myCallback", "lab2main_8py.html#a58ddcf69c594895856065fc9dcdd2c8e", null ],
    [ "rand_delay", "lab2main_8py.html#a5e85113ece04527697bfb9fcdb8c516a", null ],
    [ "aveg_tim", "lab2main_8py.html#a959c6a54b4f7066319fa2ffe980eb176", null ],
    [ "average_time", "lab2main_8py.html#a833b52359e18d748a9e7b377cb1f91a6", null ],
    [ "delta", "lab2main_8py.html#a75efc8cba1807262bfed89cdc9732e84", null ],
    [ "extint", "lab2main_8py.html#a343c77ec2ee38e640dc2c877b48c810f", null ],
    [ "myLED", "lab2main_8py.html#a8817e61d7e7ad6504c87e25b1bddda73", null ],
    [ "period", "lab2main_8py.html#a84d6b609dd0d8d9fda0ce0cda40402cd", null ],
    [ "start_count", "lab2main_8py.html#a0be6a9fbb41dc69d3ad813954bab3edb", null ],
    [ "state", "lab2main_8py.html#a1c64ea117780ec57fe22dccbd5f3c3e7", null ],
    [ "stop_count", "lab2main_8py.html#a5d4ccf207b43c02f0597d9b3c5c78e46", null ],
    [ "tim", "lab2main_8py.html#ad13467353639e6be29c803cadffca962", null ],
    [ "time", "lab2main_8py.html#adae59008db8cb2dc349e6e1c88d5f8a1", null ],
    [ "time_delay", "lab2main_8py.html#a29b1550d9ed5332a2cd8a02a6a5ac6c4", null ]
];