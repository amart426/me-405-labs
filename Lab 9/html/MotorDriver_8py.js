var MotorDriver_8py =
[
    [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ],
    [ "ch1", "MotorDriver_8py.html#acfe37823bfa3208e2660800b88bfa3f3", null ],
    [ "ch2", "MotorDriver_8py.html#ac7a88c3c43f3cf29ccb32b8338e7dfe1", null ],
    [ "ch3", "MotorDriver_8py.html#ac31ce1284196d9fb25dac24d35a7a05f", null ],
    [ "ch4", "MotorDriver_8py.html#aa8cbd5992127c00a7266adc29d69dabe", null ],
    [ "mode", "MotorDriver_8py.html#a2cf2d528509349d585dcb5235f13cb8a", null ],
    [ "moe1", "MotorDriver_8py.html#a9db326c60d70c465970abb796624d53b", null ],
    [ "moe2", "MotorDriver_8py.html#ac2231cbd9bf4f7dd64e48a69dc620d52", null ],
    [ "nfaultset", "MotorDriver_8py.html#afedbb7852e89a521ce26a6d32f0756e7", null ],
    [ "OUT_PP", "MotorDriver_8py.html#a12f932b793f3fe5aa8accff7a237d4da", null ],
    [ "pin_IN1", "MotorDriver_8py.html#ae7fab324157659601bb6e37dff60c317", null ],
    [ "pin_IN2", "MotorDriver_8py.html#a2c6d581f7aec19082cd5a0deaf06c8af", null ],
    [ "pin_IN3", "MotorDriver_8py.html#a47a20954cff9e8ec7b1c8d2018a7431c", null ],
    [ "pin_IN4", "MotorDriver_8py.html#abeb3a37b6274861ba3d9b33303cc19fc", null ],
    [ "pin_nFAULT", "MotorDriver_8py.html#a606ec75edbf27706f727a2ac51c9923e", null ],
    [ "pin_nSLEEP", "MotorDriver_8py.html#abcf033e57bd2c6b8a7ba2a7f4f36afc3", null ],
    [ "timer", "MotorDriver_8py.html#a995faa5201ef138d0bde532d2c8031be", null ],
    [ "value", "MotorDriver_8py.html#a2b795811d3e12148c1b562b2f9579f82", null ]
];