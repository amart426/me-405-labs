#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file main.py

@brief      A program is collects and send ADC readings. 
@details    This file cosists of a program that when the callback interrupt is
            is on, the prgram will use the ADC to measure and save the reading
            to an array. The interrup is connect to Pin C13 of a Nucleo L476RG
            board. The ADC is set up to read from the pin A0. This required
            using Timer 2. The program uses the UART(2) module to read and write
            data with the user interface. The program uses a Finite State Machine
            to execute this process. 

@author     Adan Martinez
@date       Feb. 4,2021
"""
from pyb import UART
import array
import pyb
import micropython

# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(1000)

## Callback function to run when timer overflows
#  timSource is the timer object triggering the callback
start_meas = 0
def myCallback(timSource):
    '''
    @brief         A function that serves as the interrupt.
    @detail        The interrupt callback function then reads the timer to see 
                    how many microseconds have elapsed since the LED was turned on.
                    The paramenter can be ignored.
    '''
    global start_meas
    start_meas = True

## Setting Up Pin for Interruptor    
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
              pyb.ExtInt.IRQ_RISING,       # Interrupt on falling edge
              pyb.Pin.PULL_NONE,           # Activate pullup resistor
              myCallback)   

adc = pyb.ADC(pyb.Pin.board.PA0)        # create an ADC on pin A2
tim = pyb.Timer(2, freq=100000)         # 100000 create a timer running at 332Hz
buf = array.array('H', (0 for index in range(1000)))  # creat a buffer to store the samples 

myuart = UART(2)

state = 1  # Starting state
while True:
    if state == 1:
        '''
        @brief         State 1: Waits for user input to begin measuring.
        @detail        The nucleo board waits for the user to send character
                       'G'. This states uses UART and its .any() method to
                       wait for the users input. The program will not move to 
                       state 2 unless 'G' is enter.
        '''
        myuart.any()                   # Waits for user to send a character.
        if myuart.any() != 0:
            val = myuart.readline()    # Read the character send by the user.
            usr = val.decode('ascii')  # Decodes the users inputs to an ascii character
            if usr == 'G':             # Checks that if the user enter 'G'
                state = 2
        else:
            state = 1                  # Returns back to state 1 if user did not enter 'G'
    elif state == 2:
        '''
        @brief         Read the ACD value when the interrupt is press
        @detail        In state 2, the program waits for the user to press
                       the interrupt. This will then get the ADC values and
                       save the reading to the buf array. Once it completes
                       all readings it moves to state 3.
        '''
        if start_meas == True:
            adc.read_timed(buf, tim)   # sample 100 values, taking 10s
            if len(buf) >= 1000:
                state = 3
    elif state == 3:
        '''
        @brief         In state 3, the data is sent back to the User Interface.
        @detail        In state 3, the program will use UART.write() to send
                       the data back to the users interface (UI_front.py). This
                       is executed in for loop for the length of the buf array.
                       When done, the program breaks the while loop.
        '''
        for n in range(len(buf)):
            myuart.write('{:},{:}\r\n'.format(n, buf[n]))
        break
