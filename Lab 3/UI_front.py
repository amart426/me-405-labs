#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file UI_front.py

@brief      A program that interacts a Nucleo L476RG board.
@details    This program purpose is to interact witha Nucleo L476RG board. This
            program uses pyserial to communicate with the board. The program
            asks the user to enter character 'G' to begin start the main.py
            program. Main.py only runs in the Nucleo Board. Main.py waits
            for the user to press the blue button to read and return data. Then
            this program will read the value and decode it to a UTF-8. The data
            is sort and save to a list. A plot is created from the data. The
            data is then saved to a csv file. 

@author     Adan Martinez
@date       Feb. 4,2021
"""
import csv
import serial
from matplotlib import pyplot as pyp

## Setting up serial port to open. Change port location if needed.
ser = serial.Serial(port='COM5',baudrate=115200, timeout=10) #use 10 for timeout

## Creating global variables
global x_counts
global Volts
## Creating lists to stored data from Nucleo Board
x_counts = []    
Volts = []
data_list = []


def sendChar():
    '''
    @brief         This function that sends the users input.
    @detail        This function asks the user enter "G", encodes the users
                   input to an ascii and then uses serial.write() to send it 
                   to the Nucleo Board.
    '''
    inv = input('Press "G" key to begin measuring: ')
    ser.write(str(inv).encode('ascii'))
       
for n in range(1):
    '''
    @detail        The first loop calls the function sendChar()
    '''
    sendChar()
    
for n in range(1000):
    '''
    @detail        This program uses a for loop to constanly read from the Nucleo
                   board. The data receive is then decode, and formated to two
                   separate lists. 
    '''
    data = ser.readline().decode('UTF-8')
    print('reading...')
    data_list = data.strip().split(',')
    x_counts.append(int(data_list[0]))
    Volts.append(int(data_list[1])*(3.3/4096))

## Setting up a plot.
pyp.title('Voltage vs. Counts')
pyp.xlabel("Number of Counts")
pyp.ylabel("Voltage [V]")
pyp.plot(x_counts, Volts, "g--")
pyp.show()
ser.close()

## Saves the Raw data to a lab3RawData.cvs file
field = ['Counts', 'Voltage [V]']
with open('lab3RawData.csv', 'w') as f:
    write = csv.writer(f)
    write.writerow(field)
    write.writerows(zip(x_counts, Volts))
    