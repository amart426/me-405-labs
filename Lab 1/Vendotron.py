#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Vendotron.py

@brief      A program that calculates the change back.
@details    This file cosists of a function getChange that has the main goal 
            to return the minumum amount of change denominations. To use this 
            calculator, the user must set a price value and the payment. For 
            the payment the user has to input the number of denominations. The 
            function will return a tuple with the return denominations. 

@author     Adan Martinez
@date       January 16,2021
"""
import math
# import keyboard  # Keyboard only works for a Windows computer. This module 
                   # allows the program to detect whenever the user presses
                   # any keyboard. 

state = 0
counter = 0


def getChange(price, payment):
    '''
    @brief         A function that calculates change back
    @detail        The function inputs are a price value and a tupple. The tuple
                   contains the denominations for the payment. The program will then
                   split the tuple and calculated the total payment. The program will
                   then take the difference between the price minus the total
                   payment. If the value of the difference is a negative value, the
                   program will return "None". Else the program will calculate the
                   calculate the return at the least number of denominations.
                   The function will then return a tuple containing the number of 
                   denomination for the total change back. The tuple goes in the 
                   order of: number of pennies, nickles, dimes, quarters, ones, 
                   fives, tens, and twenties.
              
    @param price   A variable where the total value of the item/s is stored.

    @param payment A variable that takes a tuple for the payment of the item/s.
                   The tuple has the order of denominations: number of pennies, 
                   nickles, dimes, quarters, ones, fives, tens, and twenties.
    '''
    ## Split tuple by denominations
    pennies = float(payment[0])*0.01
    nickles = float(payment[1])*0.05
    dimes   = float(payment[2])*0.10
    quarters= float(payment[3])*0.25
    ones    = float(payment[4])*1
    fives   = float(payment[5])*5
    tens    = float(payment[6])*10
    twenties= float(payment[7])*20
    # Adds all denominations
    total_payment = pennies + nickles + dimes + quarters + ones + fives + tens + twenties
    # calculates the total change value.
    change_amount = round(total_payment - price, 2)
    
    if change_amount >= 0:
        # print('Price: $' + str(price))
        # print('Payment amount: $' + str(total_payment))
        # print('Your change is $' + str(change_amount))
        frac, whole = math.modf(change_amount)
        dec =  round(frac, 2)
        chg_20 = int(whole/20)
        chg_10 = int((whole - 20*chg_20) / 10)
        chg_5  = int((whole - 20*chg_20 - 10*chg_10)/5)
        chg_1  = int(whole-(20*chg_20)-(10*chg_10)-(5*chg_5))
        # print('Twenties: ' + str(chg_20) + '  Tens: ' + str(chg_10) + '   Fives: ' + str(chg_5) + '    Ones: ' + str(chg_1))
        chg_qrt = int(dec/0.25)
        chg_dim = int((dec - 0.25*chg_qrt)/0.10)
        chg_nick= int((dec - 0.25*chg_qrt - 0.10*chg_dim)/0.05)
        chg_pen = round(((dec - 0.25*chg_qrt - 0.10*chg_dim - 0.05*chg_nick)/0.01))
        # print('Quarters: ' + str(chg_qrt) + ' Dimes: ' + str(chg_dim) + ' Nickles: ' + str(chg_nick) + ' Pennies: ' + str(chg_pen))
        min_change = (chg_pen, chg_nick, chg_dim, chg_qrt, chg_1, chg_5, chg_10, chg_20)
    elif change_amount <= 0:
        min_change = None
    else:
        print('invalid... please insert valid payment!')
    return min_change

def printWelcome():
    """
    @brief    Prints a VendotronˆTMˆ welcome message with beverage prices 
    """
    print('VendotronˆTMˆ Welcome\nInitializing...')

while True:
    '''
    @brief      Impletements the FSM using a while loop.
    @detail     Implement FSM using a while loop and an if statement will run
                eternally until user presses CTRL-C. The FSM consist of six
                states. The user can enter 'E' at any time to cancel the 
                execution. When this occurs, the program will reset to state 0.
    '''
    if state == 0:
        """
        @detail    Perform state 0 operation this init state, initializes the 
                   FSM itself, with all the code features already set up
        """
        printWelcome()
        
        state = 1 # on the next iteration, the FSM will run state 1
        
    elif state == 1:
        '''
        @detail    Display balance and wait for user to insert coins.
        '''
        print('Balance: $0.00')
        print('Insert coins in the form of: 0=penny, 1=nickle,2=dime, 3=quarter, and so on...')
        # Below line read inputs from user using map() function  
        user_input = list(map(int,input('Enter Denomination separated by a space: ').strip().split()))  #[:n] 
        #print("\nList is - ", user_input)
        num_deno = len(user_input)
        #print(num_deno)
        if num_deno == 8:
            user_payment = tuple(user_input)
            #print(user_payment)
            state = 2 # s1 -> s2
            
        elif num_deno != 8:
            print('Invalid input.')
            state = 1 # Return to state 1
        else:
            print('error... try again!')
            state = 1

    elif state == 2:
        '''
        @detail     User Selection: User makes a drink selection. The price
                    of each drink can be changed here if needed.
        '''
        print('Please make a selection: C = Cuke, P = Popsi, S = Spryte, D = Dr. Pupper')
        user_selection = str(input())
        price = 0
        if user_selection == 'C' or user_selection == 'c':
            price = float(1.00)  #Sets the price of Cuke
            state = 3 # s2 -> s3
        elif user_selection == 'P' or user_selection == 'p':
            price = float(1.20)  #Sets the price of Popsi
            state = 3 # s2 -> s3
        elif user_selection == 'S' or user_selection == 's':
            price = float(0.85)  #Sets the price for Spryte
            state = 3 # s2 -> s3
        elif user_selection == 'D' or user_selection == 'd':
            price = float(1.10)  #Sets the price of Dr. Pupper
            state = 3 # s2 -> s3
        elif user_selection == 'E':
            state = 0
        else:
            print('Please make a valid selection!')
            state = 2

    elif state == 3:
        '''
        @detail     State 3 operation is to Check Funds... If the user has 
                    input enough funds the program will transition to state 4. 
                    Else if there is insufficient funds the program will transition to state 6.
        '''
        ## Split tuple by denominations
        pennies = float(user_payment[0])*0.01
        nickles = float(user_payment[1])*0.05
        dimes   = float(user_payment[2])*0.10
        quarters= float(user_payment[3])*0.25
        ones    = float(user_payment[4])*1
        fives   = float(user_payment[5])*5
        tens    = float(user_payment[6])*10
        twenties= float(user_payment[7])*20
        # Adds all denominations
        funds = pennies + nickles + dimes + quarters + ones + fives + tens + twenties
        # calculates the total change value.
        balanced = round(funds - price, 2)
        if balanced >= 0:
            state = 4 # s3 -> s4
        elif balanced < 0:
            state = 6 # s3 -> s6
        
    elif state == 4:
        '''
        State 4 only operates if there is sufficient funds. The vendotron will
        then vend the desired drink.
        '''
        print('Vending...')
        state = 5
        
    elif state == 5:
        '''
        State 5 operations is to return the change to the user. The change is
        printed in the screen with minimun change denomination. 
        '''
        print('Change Due: ' + str(getChange(price, user_payment)))
        print('Thank You.')
        counter += 1
        state = 0
    elif state == 6:
        '''
        State 6: User has insufficient funds to make purchase. 
        Return to state 1
        '''
        print('Insufficient Funds.')
        state = 1
        
    ## The following code only work for windown. I have a mac os so could not
    # run this portion of the code. I got this code from an online source. 
    # The code is meant to detect whenever the user presses 'E' and the program
    # will stop and return to state 0.
    #    
    # elif keyboard.is_pressed('E'):
    #     '''
    #     This state only activate whenever the user enters a string 'E'. at
    #     which point the initialization message should be displayed, and the 
    #     balance should return to zero. Returns to state 0.
    #     '''
    #     print('Restarting...')
    #     state = 0
# 0 0 2 3 0 0 0 0
# C,P, & D, insufficient funds
# S good. change back 0.1 or 0 0 1 0 0 0 0 0
    
    
    
    