## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_port Portfolio Details
#  This is my ME405 Portfolio. See individual modules for detals.
#
#  Included modules are:
#  * Lab0x00 (\ref sec_lab0)
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x02 (\ref sec_lab2)
#  * Lab0x03 (\ref sec_lab3)
#  * Lab0x04 (\ref sec_lab4)
#  * Lab0x05 (\ref sec_lab5)
#  * Lab0x06 (\ref sec_lab6)
#  * Lab0x07 (\ref sec_lab7)
#  * Lab0x08 (\ref sec_lab8)
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%200/
#  * Documentation: (\ref Lab0x00 Documentation)
#
#  @section sec_lab1 Lab0x01 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/
#  * Documentation: (\ref Vendotron.py) \n
#  * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/html/
#  @subsection sec_intro Introduction
#  The project main goal is to simulate a vending machine. The program
#  consist of some user interface and change calculation. The simulation is
#  implemented as a Finite State Machine using a while loop and if statements.
#  The user can insert a payment based on money denomination. The program then 
#  ask the user to make a drink selection. The program then calculates the 
#  change back or show the balance of insufficient funds.
#  @subsection Task Diagram
#  @image html VendotronFSM.png