var classMotor_1_1MotorDriver =
[
    [ "__init__", "classMotor_1_1MotorDriver.html#a65b11389dd35fe564396b94af6b01d52", null ],
    [ "disable", "classMotor_1_1MotorDriver.html#a9ad4f746ef0e7c217ce790f7ab9260b3", null ],
    [ "enable", "classMotor_1_1MotorDriver.html#a794de1aa1bfc8ff660c75bf7f4ec8038", null ],
    [ "myCallback", "classMotor_1_1MotorDriver.html#a7f4078be5a25f2833cdf7a8cb736d6d1", null ],
    [ "set_duty", "classMotor_1_1MotorDriver.html#a420e347ab73de4a9851a5da3434ccfbb", null ],
    [ "channelA", "classMotor_1_1MotorDriver.html#ae06f0ed23bcad40f6ceea593b554df1f", null ],
    [ "channelB", "classMotor_1_1MotorDriver.html#abce6b896fd591528bd641518563fca2e", null ],
    [ "duty", "classMotor_1_1MotorDriver.html#a5944c862c6ffcc5fc6e59c74e17f4bf8", null ],
    [ "fault", "classMotor_1_1MotorDriver.html#abf3bee36c8fb4019373beef50368ea12", null ],
    [ "IN1_pin", "classMotor_1_1MotorDriver.html#a336e963c802260d78feda5ee6240af89", null ],
    [ "IN2_pin", "classMotor_1_1MotorDriver.html#ab381ffdf92b5d73b017aa0e76a593cc7", null ],
    [ "nFAULT_pin", "classMotor_1_1MotorDriver.html#a7de5b9d24c94cd167ab70569a1dd75c7", null ],
    [ "nSLEEP_pin", "classMotor_1_1MotorDriver.html#a908bf6cd362eb5d56525c5d61551bd9f", null ],
    [ "timchA", "classMotor_1_1MotorDriver.html#a87409c87c3dc21ce1d7d1d8ed9a36af3", null ],
    [ "timchB", "classMotor_1_1MotorDriver.html#afb46d42939aa9695cce0965f2445cc31", null ],
    [ "timer", "classMotor_1_1MotorDriver.html#a93dd3f6608ad30e13b49d9ac609b919d", null ],
    [ "user_input", "classMotor_1_1MotorDriver.html#a2cd1e5253347eb91300cf05a87c63b02", null ]
];