#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file MotorDriver-M.py

@brief    This file cosists of a Motor Diver class file which has the main operation to 
          control a motor when conected to arbitrary pins. 

@details  This file is meant to stricly run in micropython since it uses pyb.Timer class.
          This code purpose is to enable/disable the motor and set the duty cycle of the
          motor. It also has a callback method that when trigger, it will protect the 
          motors.

Created on Sat Nov 14 14:52:06 2020

@author: Adan Martinez
"""
import pyb
import micropython

micropython.alloc_emergency_exception_buf(200)

class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 board. 
    ''' 
    fault = False
      
    
    def __init__(self, nSLEEP_pin, nFAULT_pin, IN1_pin, IN2_pin, channelA, channelB, timer): 
        ''' 
        Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        
        @param nSLEEP_pin   A pyb.Pin object to use as the enable/disable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param channelA     A channel variable to use with IN1_pin.
        @param channelB     A channel variable to use with IN2_pin.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        '''
        print ('Creating a motor driver') 
        
        ## Creates a variable for the nSLEEP_pin object.
        self.nSLEEP_pin = nSLEEP_pin
        ## Creates a variable for timer object.
        self.timer = timer
        ## Disables the motors for safety.
        self.nSLEEP_pin.low()
        
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
                
        self.timchA = self.timer.channel(channelA, pyb.Timer.PWM, pin=self.IN1_pin)
        self.timchB = self.timer.channel(channelB, pyb.Timer.PWM, pin=self.IN2_pin)
        try:
            Extint = pyb.ExtInt(nFAULT_pin, pyb.ExtInt.IRQ_RISING, self._myCallback)  
            print('Did it')            
        except:
            pass 

    def enable(self):
        '''
        @brief       This method powers on the motor.
        '''
        if MotorDriver.fault == True:
            print("POWER OFF")
            self.nSLEEP_pin.low()
            self.user_input = input('To reset FAULT PIN please enter K: ')
            if self.user_input == 'K':
                MotorDriver.fault = False
                self.nSLEEP_pin.high()

        else:
            print ('Enabling Motor')
            ## Enables the motor.
            self.nSLEEP_pin.high()
        
    def disable(self):
        '''
        @brief   A method that powers off the motors. 
        '''
        if MotorDriver.fault == True:
            print("POWER OFF")
            self.nSLEEP_pin.low()
            self.user_input = input('To reset FAULT PIN please enter K: ')
            if self.user_input == 'K':
                MotorDriver.fault = False
                self.NSLEEP_pin.high()

        else:
            print ('Disabling Motor')
            ## Disables the motor.
            self.nSLEEP_pin.low()
        
    def set_duty(self, duty):
        ''' 
        @brief       This method sets the duty cycle to be sent to the motor to the given level. Positive values 
                     cause effort in one direction, negative values in the opposite direction.
        @param duty  A signed integer holding the duty
                     cycle of the PWM signal sent to the motor.
        '''
        ## creates a variable for the Duty cycle.
        self.duty = duty
        print('Running at ' + str(self.duty) + ' percent duty cycle.')
        ## Sets the pulse width percent equal to 0 for both channels. 
        self.timchA.pulse_width_percent(0)
        self.timchB.pulse_width_percent(0)
        
        if MotorDriver.fault == True:
            print("Motor OFF")
            self.nSLEEP_pin.low()
            self.user_input = input('To reset FAULT PIN please enter K: ')
            if self.user_input == 'K':
                MotorDriver.fault = False
                self.NSLEEP_pin.high()
     
        else:
            # Takes cares of negative duty cycle values.
            if self.duty < 0:
                print('Moving CW')
                self.timchB.pulse_width_percent(abs(self.duty))
            ## Takes care of positive duty cycles.
            elif self.duty > 0:
                print('Moving CCW')
                self.timchA.pulse_width_percent(self.duty)
            else:
                print('Error...')
                pass

    def _myCallback(self,line):
        '''
        @brief         A function that serves as the interrupt.
        @detail        The interrupt callback function then reads the timer to see 
                       how many microseconds have elapsed since the LED was turned on.
        '''
        MotorDriver.fault = True
        print('Fault Pin Trigger!')
        print(line)


if __name__ == '__main__':

    ## Any code within the if __name__ == '__main__' block will only run when 
    # the script is executed as a standalone program. If the script is 
    # imported as a module the code block will not run.
    
    ## Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)   
    pin_nFAULT  = pyb.Pin(pyb.Pin.board.PB2)
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4) 
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)
    pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1) 
    
    nfaultset = pyb.Pin(pyb.Pin.cpu.B15)
    nfaultset.init(mode=pyb.Pin.OUT_PP, value=0)
    
    ## Sets value for each channel. 
    ch1 = 1
    ch2 = 2
    ch3 = 3
    ch4 = 4
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq=20000)
    
    # Create a motor object passing in the pins and timer
    moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, pin_IN2, ch1, ch2, timer)
    moe2 = MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN3, pin_IN4, ch3, ch4, timer)
    
    # Enable the motor driver
    moe1.enable()
    moe2.enable()
    
    # Set the duty cycle at 33 percent. A positive value rotates the shaft CCW
    # and a negative value rotates the shaft CW.
    moe1.set_duty(0)
    moe2.set_duty(0)
    


    