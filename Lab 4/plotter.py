# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 08:23:45 2021

@author: matth
"""

from matplotlib import pyplot

timebuf = []
ambtemp = []
coretemp = []
mylist = []
with open("TempOutput.csv", "r") as tempout:
    for n in range(510):
        row = tempout.readline()
        if n > 1:
            mylist = row.strip('\n').split(',')
            timebuf.append(float(mylist[0])/60)
            ambtemp.append(float(mylist[1]))
            coretemp.append(float(mylist[2]))


pyplot.figure
pyplot.plot(timebuf,ambtemp)
pyplot.plot(timebuf,coretemp)
pyplot.legend(['Ambient Temperature','Core Temperature'])
pyplot.ylim(55,80)
pyplot.xlim(0,8.5)
pyplot.title('Ambient and Core Temperature')
pyplot.xlabel('Time [Hours]')
pyplot.ylabel('Temperature [F]')