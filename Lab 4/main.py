'''
@file main.py

@brief      Reads and records the temperature from a MCP9808 sensor and MCU.
@details    This file imports the mcp9808 module to take temperature 
            readings approximately every sixty seconds from the STM32 and from 
            the MCP9808, and saves those readings in a CSV file on the 
            microcontroller. The CSV file contains the time that the measurement 
            was taken, STM32 (sensor) temperature in Fahrenheit, and ambient 
            (MCP9808) temperature in Farenheit. Thr data recording stops when 
            the user presses Ctrl-C.

@author Matthew Pfeiffer
@author Adan Martinez

@date Feb. 8, 2021
'''
## Importing modules
import pyb
import array
import utime
import coretemp as ins
import mcp9808 as amb
from pyb import I2C
from matplotlib import pyplot as pyp

## Initializes the state
state = 0

while True:
    try:
        if state == 0:
            ## Runs state 0
            i2c = pyb.I2C(1)  # I2C created on bus 1
            addr = 24               # Specfies the address for the I2C comunication.
            amb.init(i2c,addr)           # Feeds the bus number and address number for initializing the mcp9808 module.
            if amb.check() == True:      # Only runs when the amb.check() function returns a True value.
                state = 1                # A timestamp is also added to the csv file. The program then moves to state 2.
                pernum = 0       
                starttime = utime.ticks_ms()   # Timestamp for data initial reading.
                time = 0             # Creates objects for handling data overflow
                period = 0
                stoval = []
                ## The following two lines create a TemmPOut.csv file and writes headers for the columns as; The timestamp, ambient 
                # temperature, and the Core Temperature]
                with open("TempOutput.csv","w") as tempout:                
                    tempout.write('Time [min], Ambient Temperature [F], Core_Temperature[F],\n\n')
        elif state == 1:
            ## Runs state 1
            while (time/60000) <= 510:                  ## Only runs when the time is under 8:30 [hrs:mins]
                if utime.ticks_ms()-starttime == 0:     #  Takes care of overflow. 
                    if pernum != 0:
                        period = time*pernum
                        pernum += 1
                    else:
                        pernum = 1
                time = period+utime.ticks_ms()-starttime                               
                with open("TempOutput.csv", "a") as tempout:
                    ## The following three lines write the recorded temperature and timestamp to the csv file.
                    tempout.write("{:.2f}".format((time)/60000) 
                                  + ', ' + "{:.2f}".format(amb.fahrenheit()) + ', ' 
                                  + "{:.2f}".format(ins.readtemp()) + ',\n')
                    utime.sleep(60)         ## The program then goes to sleep for 1minute before takinng another loop
            print('\nAll Complete :)\n')    ## A break only occurs when the time as exceed the 8hr and 30 minutes.
            break
            
    except KeyboardInterrupt:                   ## Takes care of ending the program whenever the user enters CTRL-C
        print('\nProgram Ended By User\n')
        break
    