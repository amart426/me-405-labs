## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#
#  @section sec_port Portfolio Details
#  This is my ME405 Portfolio. See individual modules for detals.
#
#  Included modules are:
#  * Lab0x00 (\ref sec_lab0)
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x02 (\ref sec_lab2)
#  * Lab0x03 (\ref sec_lab3)
#  * Lab0x04 (\ref sec_lab4)
#  * Lab0x05 (\ref sec_lab5)
#  * Lab0x06 (\ref sec_lab6)
#  * Lab0x06 (\ref sec_lab7)
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%200/
#  * Documentation: \ref Lab0
#
#  @section sec_lab1 Lab0x01 Documentation
#  * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/Vendotron.py
#  * Documentation: Vendotron.py\ref Lab 1
#  * HMTL: Lab1 https://bitbucket.org/amart426/me-405-labs/src/master/Lab%201/html/
#
#  @section sec_lab2 Lab0x02 Documentation
#  * Source: https://bitbucket.org/amart426/amart426.bitbucket.io/src/master/me_405/
#  * Documentation: Lab 2 (lab2main.py\ref)
#  * HMTL: Lab2 https://bitbucket.org/amart426/me-405-labs/src/master/Lab%202/html/
#
## @section sec_lab3 Lab0x03 Documentation
# * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%203/
# * Documentation: main.py\n
# * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%203/html/
#
#
# @section sec_intro3 Lab3: Introduction
#  The project main goal is to measure voltage in a board and sennd it the PC. This
#  project consisted of runnig a Nucleo-L476RG with a main.py and UI_front.py code to interact
#  with the user. The main.py script  waits for the user to send character 'G'
#  and then it will get ready to start measuring.
#  The user must press the blue button connected to Pin C13 to start
#  saving the voltage in an array. When finish measuring, the array 
#  will then be send back to the PC or UI_front.py.
#  The UI_front then receive and store the data send via serial port. The
#  final result is a plot of the data.
#  @subsection sec_notes Comments
#  In the process of completing this lab I had some issues getting good data
#  from the adc. When I press the blue button, it starts reading from a high value. 
#  I try using different pins. The best one was Pin A0. This would give a better
#  a smoother curve. After doing all of changes and playing around with different
#  pins and timers and frequencies. My UI_front.py receive empy data. My UI_front
#  receive empy data. I not sure if my I mess my Nucleo but now is not respoding. 
#
## @section sec_lab4 Lab0x04 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab04/
# * Documentation: main.py & mcp9808.py\n
#
#  @subsection sec_intro4 Lab4: Introduction
#  The main goal of this project is to measure the ambient temperature and 
#  the STM32 Microcontroller core temperature. The ambient temperature is measure
#  using a MCP9808 sensor. The pyb.I2C() allows the interface between the
#  the sensor and the STM32 Microcontroller. The physical connnection is shown
#  below. The microcontroller core temperature is measure
#  using the pyb.ADCAll() method. The main.py file runs for 8hrs and 30 mins
#  and records the time, ambient temperature, and core temperature and finally
#  saves the data to a csv file. The data from the csv file is plotted
#  separately.
#  @subsection sec_notes Comments
#  One big obstacle for completing this assignment was having the MCP9808 sensor
#  arrive late. One team member had the MCP9808 sensor for the time being, therefore
#  one member collected all the data for the sake of time. The other was in
#  in charged for documentating. Other issues included the computer powering
#  down and restarting collection data. The data was also plotted separately. This
#  facilitated data and plot formating. 
#  @subsection  sec_phycon Physical-Connection
#  @image html SensorCon.png 
#  @subsection  sec_plots  Temperature vs.Time Plot
#  The plot below shows the ambient and internal temperatures as functions of time.
#  This test was performed beginning at 5:53 PM in Matthew Pfeiffer's bedroom. Data
#  points were taken each minute for 510 minutes.
#  @image html  TempPlot.png
#  @author Adan Martinez 
#
#  @date January 26, 2021
#
#
## @section sec_lab5 Lab0x05 Documentation
# * Source: https://bitbucket.org/mbpfeiff/pfeiffer-martinez-shared/src/master/Lab05/
# * Documentation: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%205/
# * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%205/html/
#  @subsection sec_intro Introduction
#  The main goal of this project is to analyze a balancing platform. The 
#  mechanics of the system consists of two motors and each connected to a 
#  lever arm. A pushing rod connects the lever arm to the platform. When a 
#  torque is applied, the lever arm rotates, and the motion is transfer to the 
#  platform via the pushing rod. For the analysis the ball is taken into 
#  account. The analysis takes part by solving for kinetic equations that 
#  relates the motion of the lever arm pivoting point to the base of the 
#  platform. To approach the following assumptions were made. The system was 
#  model as a pair of balancing beams that each move in one degree of freedom.
#  We assume small angle approximation. We assume the platform has the most 
#  significant mass and inertia and ignore that of the lever arm and push rod.
#  For the ball we assume it rolls without slipping.  The end result is two 
#  high order differential equations relating the torque from the motor, ball's
#  acceleration, and angular acceleration of the platform. Also, the velocities 
#  and position of the platform and lever arm.  The equations are then 
#  manipulated to a matrix form. Please refer to Analysis page for further 
#  detail. 
#  @author Matthew Pfeiffer
#  @author Adan Martinez 
#  @date February 16, 2021
#
#
#
## @section sec_lab6 Lab0x06 Documentation
# * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%206/
# * Documentation: \ref Lab0x06 Documentation
# * HMTL: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%206/html/
#  @subsection sec_intro Introduction
#  The main goal of this project is to analyze a balancing platform. The 
#  mechanics of the system consists of two motors and each connected to a 
#  lever arm. A pushing rod connects the lever arm to the platform. When a 
#  torque is applied, the lever arm rotates, and the motion is transfer to the 
#  platform via the pushing rod. For the analysis the ball is taken into 
#  account. The analysis takes part by solving for kinetic equations that 
#  relates the motion of the lever arm pivoting point to the base of the 
#  platform. To approach the following assumptions were made. The system was 
#  model as a pair of balancing beams that each move in one degree of freedom.
#  We assume small angle approximation. We assume the platform has the most 
#  significant mass and inertia and ignore that of the lever arm and push rod.
#  For the ball we assume it rolls without slipping.  The end result is two 
#  high order differential equations relating the torque from the motor, ball's
#  acceleration, and angular acceleration of the platform. Also, the velocities 
#  and position of the platform and lever arm.  The equations are then 
#  manipulated to a matrix form. Please refer to Analysis page for further 
#  detail. 
#  The equation derived in lab5 are then linearize using the jacobian method
#  implemented with MATLAB. The equations are then put into a state space form.
#  We then simulate the time response of the state space model using first an
#  open loop and then closed loop with a gain. To complete this task we use 
#  MATLAB and Simulink. Please refer to the Simulation
#  page for through detail for each simulation and results. 
#  @author Adan Martinez 
#
#  @date February 22, 2021
#
## @section sec_lab7 Lab0x07 Documentation
# * Source: https://bitbucket.org/amart426/me-405-labs/src/master/Lab%207/
# * Documentation: \ref Lab0x07 Documentation
# * HMTL: https://amart426.bitbucket.io/me_405/Lab0x07
#
#  @subsection sec_intro Introduction
#  The main goal of this project is read the position of a ball on a resistive
#  touch panel. The first step was to connect the physical connections. There
#  are three pieces to this connection. The douple pin needs to be solder to the
#  PCB board first. Then, the touch panel is adhere to the top plate of the 
#  platform. The other, and crucial connection is wiring the analog module
#  to the PCB board. This corresponds to the X16 pins location. The second
#  and most crucial step, is to examine the pin connections and know to which
#  are the touch panel's x-,x+,y-,and y+ pins are to the A7, A6, A1, and A0 pins.
#  The ER-TFT080-1 resistive touch panel data sheet was use to localize the
#  the wiring of the touch panel. The last step is to write a hardware driver 
#  to interface to the resistive touch panels with the STM32 microcontroller. 
#  For detail of the code execution please refer to the soucer link. 
#  @subsection  sec_comments  Comments
#  One of the requirements was to read all the position under 500 microseconds. 
#  I had trouble reaching this goal. This fastest that I got my program running was
#  1195 microseconds. Another issue I had was that the code will not update after
#  I updated the scripts. To make it work, I had to either eject and connect 
#  the PCB board constantly or diconnect the PCB board constantly. Running the
#  each method individually gives a batter result of the readings. 
#  @image html lab7.png
#  @subsection  sec_con Connection&Pins
#  @image html pins.JPG
#  @image html wiring.JPG
#
#  @author Adan Martinez 
#
#  @date March 1, 2021
 
############
# @page page2 Simulation
#  @subsection OpenLoopModel
#  3a) For the first simulation the ball is set initially at rest on a 
#  level platform directly above the center of gravity of the platform and 
#  there is no torque input from the motor. This simulation was run for 1 [s]. 
#  The time response of the x, theta, x_dot, and theta_dot are plotted in separate
#  plots. These plots are accurate since the ball is at its equilibrium point
#  and there is no motor torque detected. We expect the system to behave as its
#  shown.
#  @image html 3a.png
#  3b) For the second simulation, the ball is set initially at rest on a level 
#  platform offset horizontally from the center of gravity of the platform 
#  by 5 [cm] and there is no torque input from the motor. The simulation elapsed
#  for 0.4 [s]. Analyzing the plots we see that for the position(x) plot, the 
#  the ball is at a 50mm positive offset in the vertical direction. The ball
#  is release from rest and this coincides with our velocities plots(x_dot & 
#  theta_dot) which start at zero and increase with time. The angle of the
#  platform also increases due to the weigth of the ball. The ball weigth is 
#  small compare to that of the platform so it causes a small theta angle. 
#  @image html 3b.png
#  3c) The next simulation consists of having the ball initially at rest on a 
#  platform inclined at 5◦ directly above the center of gravity of the platform 
#  and there is no torque input from the motor. This simulation runs for 0.4 [s].
#  The plots shows that we start a 5deg angle position and at rest. This does
#  correlate with our prediction that the curves should have a similar time
#  response curve. 
#  @image html 3c.png
#  3d) The next simulation consisted of having the ball initially at rest on 
#  a level platform directly above the center of gravity of the platform and 
#  there is an impulse1 of 1 mNm · s applied by the motor. Run this simulation 
#  for 0.4 [s]. 
#  @image html 3d.png
#  @subsection ClodeLoopModel
#  4a) For modeling a closed loop system, we use Simulink to set up our tranfer
#  function and implemented a gain K = [-0.05 -0.02 -0.3 -0.2]. Also, the 
#  simulation in closed-loop by implementing a regulator using full state 
#  feedback. The ball is initially at rest. The simulation runs for 20[sec].
#  Our system time response is not behaving as expected. The system curves
#  should at one point approach zero 
#  @image html 4a.png
#  @image html bd.png
# 
#### @page page1 Analysis
#   @image html Analysis.jpeg
#   @image html Analysis1.jpeg
#   @image html Analysis2.jpeg
#   @image html Analysis3.jpeg
#   @image html Analysis4.png
#   @image html Analysis5.png
#   @image html Analysis6.jpg
######




